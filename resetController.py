import cherrypy
import re, json
from stats_library import _stats_database

class ResetController(object):

    def __init__(self, sdb=None):
        if sdb is None:
            self.sdb = _stats_database()
        else:
            self.sdb = sdb


    def PUT_INDEX(self):
        '''when PUT request comes in to /reset/ endpoint, then the stats database is reloaded'''
        output = {'result':'success'}

        try:
            self.sdb.__init__()
            self.sdb.load_stats('stats.dat')
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)

    def PUT_KEY(self, year):
        '''when PUT request comes in for /reset/year endpoint, then that stats is reloaded and updated in sdb'''
        output = {'result':'success'}
        year = int(year)

        try:
            # data = json.loads(cherrypy.request.body.read().decode())

            sdbtmp = _stats_database()
            sdbtmp.load_stats('stats.dat')

            stats = sdbtmp.get_stats(year)

            self.sdb.set_stats(year, stats) #TODO remember to reset genre also


        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)

        return json.dumps(output)
