//minteri2 jmantic2
console.log('page load - entered main.js for js');

var yearArray = false;
var addYearButton = document.getElementById('add-year');
addYearButton.onmouseup = addYear;

var first = true;
var yearsAr = [];
var recAr = [];
var srsAr = [];
var ptsForAr = [];
var ptsAgtAr = [];


var submitButton = document.getElementById('send-button');
submitButton.onmouseup = getFormInfo;

var clearButton = document.getElementById('clear-button');
clearButton.onmouseup = resetAll;

function addYear(){
  var form = document.getElementById('omg');
  input = document.createElement('input');
  input.setAttribute('type', 'text');
  input.setAttribute('id', 'year-text');
  input.setAttribute('class', 'form-control')
  form.appendChild(input);
  yearArray = true;

}

function getFormInfo(){
    console.log('entered getFormInfo!');
    var all;
    var message = null;

    if (yearArray){
      all = false;
      if (first){
        var years = document.querySelectorAll("[id='year-text']");
        p = document.getElementById("description-add-years");
        p.innerHTML = "Add more years for comparison";
        var i;
        for (i = 0; i < years.length; i++){
          yearsAr.push(years[i].value);
        }
      }

    }
    else{
      all = true;
    }
    var form = document.getElementById('omg');
    form.innerHTML = "";
    document.getElementById("add-year").disabled = true;
    p = document.getElementById("description-add-years");
    p.innerHTML = "";




    if (first){
      var form = document.getElementById('select-stat-div');
      label = document.createElement('label');
      label.setAttribute('for', 'checkboxes');
      label.innerHTML = 'Select Stat to Sort Data By';
      select = document.createElement('select')
      select.setAttribute('class', 'form-control')
      select.setAttribute('id', 'select-stat');
      option = document.createElement('option');
      option.innerHTML = 'Year';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Record';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'SRS (Simple Rating System)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points For';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points Against';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Record (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'SRS (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points For (ONLY)';
      select.appendChild(option);
      option = document.createElement('option');
      option.innerHTML = 'Points Against (ONLY)';
      select.appendChild(option);
      form.appendChild(label);
      form.appendChild(select);

      submitButton.innerHTML = "Sort";
    }







    var server_addr = "http://student10.cse.nd.edu";
    console.log('Selected Server: ' + server_addr);

    var port = "51067";
    console.log('Port Number: ' + port);

    var request_type = "GET";

    var request_stat = document.getElementById('select-stat').value;
    var i;
    var single;
    var singleStat;
    var stat;
    if (request_stat == "Year"){
      yearsAr.sort();
      yearsAr.reverse();

    }
    else if(request_stat == "Record"){
      sort_stat('r');
      single = false;
    }
    else if(request_stat == "SRS (Simple Rating System)"){
      sort_stat('s');
      single = false;
    }
    else if(request_stat == "Points For"){
      sort_stat('pf');
      single = false;
    }
    else if(request_stat == "Points Against"){
      sort_stat('pa');
      single = false;
    }
    else if(request_stat == "Record (ONLY)"){
      yearsAr.sort();
      yearsAr.reverse();
      sort_stat('r');
      single = true;
      singleStat = "Record";
      stat = "record";
    }
    else if(request_stat == "SRS (ONLY)"){
      yearsAr.sort();
      yearsAr.reverse();
      sort_stat('s');
      single = true;
      singleStat = "SRS";
      stat = "srs";
    }
    else if(request_stat == "Points For (ONLY)"){
      yearsAr.sort();
      yearsAr.reverse();
      sort_stat('pf');
      single = true;
      singleStat = "Points For";
      stat = "points_for";
    }
    else{
      yearsAr.sort();
      yearsAr.reverse();
      sort_stat('pa');
      single = true;
      singleStat = "Points Against";
      stat = "points_against";
    }
    if (!single){
      recAr = [];
      srsAr = [];
      ptsForAr = [];
      ptsAgtAr = [];
      createFullTable();
      if (!all){
        var key = null;

        for(i = 0; i < yearsAr.length; i++){
          key = yearsAr[i];
          makeRequest(server_addr, port, request_type, key, message);
          sleep(50);
        }

      }
      else{
        yearArray = true;
        makeRequest(server_addr, port, request_type, key, message);
      }
    }
    else{

      createPartialTable(singleStat);
      if (!all){
        var key = null;
        for(i = 0; i < yearsAr.length; i++){
          key = yearsAr[i];
          makeRequestSingle(server_addr, port, request_type, key, stat, message);
          sleep(50);
        }
      }
      else{
        yearArray = true;
        makeRequestSingle(server_addr, port, request_type, key, stat, message);
      }
    }

} // end of get form info

function makeRequest(server_addr, port, request_type, key, message){
    var xhr = new XMLHttpRequest();

    var url = server_addr + ":" + port + "/stats/";
    if(key) {
        url += "year/" + key;
    }

    console.log("Making request to:" + url);
    xhr.open(request_type, url, true);

    if (key){
      xhr.onload = function(e) {
          updateText(xhr.responseText, key);
      }
    }
    else{
      xhr.onload = function(e) {
          updateTextAll(xhr.responseText);
      }
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(message)
}

function updateText(response_text, year){
  if(response_text[12] == 's'){
    var record = "";
    var srs = "";
    var points_for = "";
    var points_against = "";
    var i = 47;
    while (response_text[i] != "\""){
      record += response_text[i];
      i++;
    }
    recAr.push(record);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      srs += response_text[i];
      i++;
    }
    srsAr.push(srs);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      points_for += response_text[i];
      i++;
    }
    ptsForAr.push(points_for);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != "}"){
      points_against += response_text[i];
      i++;
    }
    ptsAgtAr.push(points_against);



    var table = document.getElementById("table");
    var tr = document.createElement("tr");
    table.appendChild(tr)
    var td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = record;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = srs;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_for;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_against;
    tr.appendChild(td);
  }
  else{
    var index = yearsAr.indexOf(year);
    yearsAr.splice(index, 1);
  }
}

function updateTextAll(response_text){
  var i = 39;
  var year;
  var record;
  var srs;
  var points_for;
  var points_against;
  var table = document.getElementById("table");
  var tr;
  var td;
  while (response_text[i] != "]"){
    year = "";
    record = "";
    srs = "";
    points_for = "";
    points_against = "";
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ','){
      year += response_text[i]
      i++
    }
    yearsAr.push(year);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    i++;
    while (response_text[i] != "\""){
      record += response_text[i];
      i++;
    }
    recAr.push(record);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      srs += response_text[i];
      i++;
    }
    srsAr.push(srs);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != ","){
      points_for += response_text[i];
      i++;
    }
    ptsForAr.push(points_for);
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    while (response_text[i] != "}"){
      points_against += response_text[i];
      i++;
    }
    ptsAgtAr.push(points_against);
    i++;
    tr = document.createElement("tr");
    table.appendChild(tr)
    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = record;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = srs;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_for;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = points_against;
    tr.appendChild(td);
    }
}

function makeRequestSingle(server_addr, port, request_type, key, stat, message){
    var xhr = new XMLHttpRequest();

    var url = server_addr + ":" + port + "/stats/single/" + stat;
    if(key) {
        url += "/" + key;
    }

    console.log("Making request to:" + url);
    xhr.open(request_type, url, true);


    xhr.onload = function(e) {
        updateTextSingle(xhr.responseText, stat, key);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(message)
}

function updateTextSingle(response_text, stat, year){
  if(response_text[12] == 's'){
    var i = 35;
    var single = "";
    while (response_text[i] != ":"){
      i++;
    }
    i++;
    i++;
    if (stat == "record"){
      i++;
      while (response_text[i] != "\""){
        single += response_text[i];
        i++;
      }
    }
    else{
      while(response_text[i] != "}"){
        single += response_text[i];
        i++
      }
    }

    var table = document.getElementById("table");
    var tr = document.createElement("tr");
    table.appendChild(tr)
    var td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = year;
    tr.appendChild(td);

    td = document.createElement("td");
    td.setAttribute("style", "width : %");
    td.innerHTML = single;
    tr.appendChild(td);

  }
  else{
    var index = yearsAr.indexOf(year);
    yearsAr.splice(index, 1);
  }
}


function sort_stat(request){
  var i;
  var sorting = [];
  if (request == 'r'){
    var record;
    var w;
    var l;
    var t;
    var rec;
    for (i = 0; i < yearsAr.length; i++){
      record = recAr[i].split('-')
      w = parseFloat(record[0]);
      l = parseFloat(record[1]);
      t = parseFloat(record[2]);
      rec = (w + t/2)/(w + l + t);
      sorting.push({'year': yearsAr[i], 'rec': rec, 'w': w});
    }
    sorting.sort(function(a, b) {
      return ((a.rec < b.rec) ? -1 : ((a.rec == b.rec) ?  ((a.w < b.w) ? -1 : ((a.w == b.w) ? -1 : 1)): 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }
  else if (request == 's'){
    var srs;
    for (i = 0; i < yearsAr.length; i++){
      srs = parseFloat(srsAr[i]);
      sorting.push({'year': yearsAr[i], 'srs': srs});
    }
    sorting.sort(function(a, b) {
      return ((a.srs < b.srs) ? -1 : ((a.srs == b.srs) ? -1 : 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }
  else if (request == 'pf'){
    var pf;
    for (i = 0; i < yearsAr.length; i++){
      pf = parseInt(ptsForAr[i]);
      sorting.push({'year': yearsAr[i], 'pf': pf});
    }
    sorting.sort(function(a, b) {
      return ((a.pf < b.pf) ? -1 : ((a.pf == b.pf) ? -1 : 1));
      });
    sorting.reverse();
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }
  else if (request == 'pa'){
    var pa;
    for (i = 0; i < yearsAr.length; i++){
      pa = parseInt(ptsAgtAr[i]);
      sorting.push({'year': yearsAr[i], 'pa': pa});
    }
    sorting.sort(function(a, b) {
      return ((a.pa < b.pa) ? -1 : ((a.pa == b.pa) ? -1 : 1));
      });
    for (i = 0; i < yearsAr.length; i++){
      yearsAr[i] = sorting[i].year;
    }
  }
}

function createFullTable(){
  var table_div = document.getElementById("table-div");
  table_div.innerHTML = "";
  var table = document.createElement("table");
  table.setAttribute("style", "width: 100%");
  table.setAttribute("class", "table table-striped");
  var thead = document.createElement("thead");
  table.appendChild(thead);
  var tr = document.createElement("tr");
  thead.appendChild(tr);
  var th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "Year";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "Record";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "SRS";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "Points For";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "Points Against";
  tr.appendChild(th);

  tbody = document.createElement('tbody');
  tbody.setAttribute("id", "table")
  table.appendChild(tbody);
  table_div.appendChild(table);

  if(first){
    first = false;
  }
}

function createPartialTable(stat){
  var table_div = document.getElementById("table-div");
  table_div.innerHTML = "";
  var table = document.createElement("table");
  table.setAttribute("style", "width: %");
  table.setAttribute("class", "table table-striped");
  var thead = document.createElement("thead");
  table.appendChild(thead);
  var tr = document.createElement("tr");
  thead.appendChild(tr);
  var th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = "Year";
  tr.appendChild(th);

  th = document.createElement("th");
  th.setAttribute("style", "width : %");
  th.innerHTML = stat;
  tr.appendChild(th);

  tbody = document.createElement('tbody');
  tbody.setAttribute("id", "table")
  table.appendChild(tbody);
  table_div.appendChild(table);
}

function resetAll(){
  yearArray = false;
  first = true;
  var resetSelect = document.getElementById('select-stat-div');
  resetSelect.innerHTML = "";
  var resetYears = document.getElementById('omg');
  resetYears.innerHTML = "";
  var resetTable = document.getElementById("table-div");
  resetTable.innerHTML = "";
  var resetMessage = document.getElementById("description-add-years");
  resetMessage.innerHTML = "Add the years for which you wish to compare stats. If none are added, data for all years will be displayed. Also, no invalid years will be taken into account.";
  document.getElementById("add-year").disabled = false;
  submitButton.innerHTML = "Search";
  yearsAr = [];
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
